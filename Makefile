DESTDIR ?= /usr/local/bin

compile-server:
	cd src/server && go build

install-server:
	install -Dm 755 ./src/server/pntp $(DESTDIR)/pntp

install-client:
	install -Dm 755 ./src/client/pntp_set.fnl $(DESTDIR)/pntp_set.fnl

install-openrc:
	install -Dm 755 ./init/openrc/pntp /etc/init.d/
