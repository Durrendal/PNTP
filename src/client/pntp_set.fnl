#!/usr/bin/fennel
(local json (require "cjson"))
(local http (require "socket.http"))

(fn setPNTP [curpntp]
  "Convert PNTP table to date format, then invoke date -s and hwclock --systohc"
  (let
      [datestring (.. (. curpntp "Month") "/" (. curpntp "Day") "/" (. curpntp "Year") " " (. curpntp "Hour") ":" (. curpntp "Minute") ":" (. curpntp "Second"))
       cmds [(.. "date -s \"" datestring "\"")
             "hwclock --systohc"]
       sh (io.popen "/bin/sh" "w")]
    (each [k v (pairs cmds)]
      (sh:write (.. v "\n")))
    (sh:close)))

(fn decodePNTP [curpntp]
  "Convert json to lua table"
  (json.decode curpntp))

(fn getPNTP [uri]
  "Handle HTTP GET"
  (let
      [(body resp) (http.request uri)]
    (if (= resp 400)
        (do
          (io.stderr:write "Bad request encountered\n")
          (os.exit))
        (= resp 404)
        (do
          (io.stderr:write "PNTP handler not found.\n")
          (os.exit))
        (= resp "connection refused")
        (do
          (io.stderr:write "Cannot contact PNTP server.\n")
          (os.exit))
        (= resp 200)
        (do
          (setPNTP (decodePNTP body))))))

;;(getPNTP "http://192.168.88.123:12380")
(getPNTP ...)
