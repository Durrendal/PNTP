#!/bin/ash
PNTPSRV="http://lambdacreate.com:12380"

#jq 'map(.Notes |= gsub("\n"; "\n\n"))' <- this might be better.

NTP=$(curl "$PNTPSRV" 2>/dev/null)
Month=$(echo $NTP | jq .Month)
Day=$(echo $NTP | jq .Day)
Year=$(echo $NTP | jq .Year)
Hour=$(echo $NTP | jq .Hour)
Minute=$(echo $NTP | jq .Minute)
Second=$(echo $NTP | jq .Second)
CT="$Month/$Day/$Year $Hour:$Minute:$Second"

date -s "$CT"
hwclock --systohc
