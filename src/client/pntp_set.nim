import os, osproc, httpclient, json, strutils
#nim c -d:release -d:ssl pntp_set.nim

#pntp_set https://lambdacreate.com:12380
proc setPNTP() : string =
  if paramCount() < 1:
    return "Please specify the PNTP server."
    
  var
    client = newHttpClient()
  let
    srvr = paramStr(1)
    pntpjson = client.getContent($srvr).strip()
    pntp = parseJson(pntpjson)
    month = pntp["Month"]   # <- surely there's a better way to unmarshal json values?
    day = pntp["Day"]
    year = pntp["Year"]
    hour = pntp["Hour"]
    minute = pntp["Minute"]
    second = pntp["Second"]
    datecmd = "date -s \"" & $month & "/" & $day & "/" & $year & " " & $hour & ":" & $minute & ":" & $second & "\""
    hwclockcmd = "hwclock --systohc"

  #execCmd returns the retcodes for checks, echo on the proc also produces the output from the exec.
  if execCmd(datecmd) == 0:
     if execCmd(hwclockcmd) == 0:
        return "Time syncrhonized with PNTP"
     else:
       return "Hwclock sync failed!"
  else:
    return "Date preparation failed!"

echo setPNTP()
