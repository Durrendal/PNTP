package main

import (
	"fmt"
	"log"
	"time"
	"net/http"
	"encoding/json"
)

type ntp struct {
	Month time.Month `json:"Month"`
	Day int `json:"Day"`
	Year int `json:"Year"`
	Hour int `json:"Hour"`
	Minute int `json:"Minute"`
	Second int `json:"Second"`
}

type yyyymmdd struct {
	Year int `json:"Year"`
	Month time.Month `json:"Month"`
	Day int `json:"Day"`
}

type hhmmss struct {
	Hour int `json:"Hour"`
	Minute int `json:"Minute"`
	Second int `json:"Second"`
}

func curtime() *ntp {
	pntp := ntp{}
	ct := time.Now()
	pntp.Month = ct.Month()
	pntp.Day = ct.Day()
	pntp.Year = ct.Year()
	pntp.Hour = ct.Hour()
	pntp.Minute = ct.Minute()
	pntp.Second = ct.Second()
	return &pntp
}

func curyyyymmdd() *yyyymmdd {
	yyyymmdd := yyyymmdd{}
	ct := time.Now()
	yyyymmdd.Month = ct.Month()
	yyyymmdd.Day = ct.Day()
	yyyymmdd.Year = ct.Year()
	return &yyyymmdd
}

func curhhmmss() *hhmmss {
	hhmmss := hhmmss{}
	ct := time.Now()
	hhmmss.Hour = ct.Hour()
	hhmmss.Minute = ct.Minute()
	hhmmss.Second = ct.Second()
	return &hhmmss
}

func PNTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Poorman's NTP Requested")
	
	if r.URL.Path == "/" {
		pntp := curtime()
		json.NewEncoder(w).Encode(pntp)
	} else if r.URL.Path == "/Hour" {
		pntp := curtime()
		json.NewEncoder(w).Encode(pntp.Hour)
	} else if r.URL.Path == "/Minute" {
		pntp := curtime()
		json.NewEncoder(w).Encode(pntp.Minute)
	} else if r.URL.Path == "/Second" {
		pntp := curtime()
		json.NewEncoder(w).Encode(pntp.Second)
	} else if r.URL.Path == "/YYYY-MM-DD" {
		yyyymmdd := curyyyymmdd()
		json.NewEncoder(w).Encode(yyyymmdd)
	} else if r.URL.Path == "/HH:MM:SS" {
		hhmmss := curhhmmss()
		json.NewEncoder(w).Encode(hhmmss)
	}
}

func handleReq() {
	http.HandleFunc("/", PNTP)
	log.Fatal(http.ListenAndServe(":12380", nil))
}

func main() {
	handleReq()
}
