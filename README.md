# What?
A Poorman's NTP Server.

## Seriously, Why?
Because I have a few arm SBC that have semi-broken hwclocks, they can be set, but time doesn't actually persist, and they'll jump randomly into the future by a number of years on what appears whim, and most frustratingly which refuse to remain in sync with a proper NTP service. PNTP is a stop gap so that I can continue to use the boards as part of my CI/CD builders while investigating a real fix for the issue.

## Building
You should only need to install golang and make to install the server. 
```
make compile-server
```

or by hand
```
cd src/go && go build
```

## Installing
You can install the client like so, but this is really just sticking them in /usr/local/bin. Feel free to put them wheresoever makes sense.
```
make install-server
make install-client
```

The fennel client relies on Fennel, lua5.3-cjson, & lua5.3-socket.

## Server
PNTP consists of a simple server service which runs on a properly synced and functioning computer. It consists of a single handle to return the current time in the following format.
```
{
  "Month": 2,
  "Day": 10,
  "Year": 2022,
  "Hour": 22,
  "Minute": 54,
  "Second": 37
}
```

PNTP doesn't currently support logging, but it likely will at some point if this issue persists.

PNTP is statically configured to listen on port 12380.

And OpenRC script for PNTP can be found in the init/openrc directory of this repo and setup like so.
```
make install-openrc
sudo rc-update add pntp default
sudo rc-service pntp start
```

Or you can run it as a cronjob at every reboot
```
@reboot sleep 60 && /usr/loca/bin/pntp
```

## Client
Technically no client is really needed, you could throw together something with a bit of shell, curl -s | jq is sufficient for such a simple return. I wrote a simple client to query PNTP and pipe in json into date -s && hwlcock --systohc to keep the time on the boards in sync.

That's configured as a simple cronjob
```
*/15 * * * * /usr/local/bin/pntp_set.fnl "http://192.168.88.123:12380"
```

and when it runs it converts the pntp json into the following calls:
```
date -s "2/10/2022 22:54:37"
hwlock --systohc
```

If you'd like to filter time information, the PNTP server supports the following urls. 

```
/             <- return everything
/Hour         <- return the current hour
/Minute       <- return the current minute
/Second       <- return the current second
/YYYY-MM-DD   <- return the current year, month, and day
/HH:MM:SS     <- return the current hour, minute, and second
```